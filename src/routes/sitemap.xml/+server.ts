const now = new Date(Date.now())
// YYYY-MM-DDThh:mm:ssTZD
const lastModified = `${now.getFullYear()}-${now.getMonth()}-${now.getDate()}T${now.getHours()}:${now.getMinutes()}.${now.getSeconds()}+00:00`;
const baseUrl = `https://higenku.org`
const generateUrl = (path: string) => 
        `<url>\n` +
        `<loc>${baseUrl}${path}</loc>\n` +
        `</url>\n`

const getSiteMap = () => {

    const urls = Object.keys(import.meta.glob('/src/routes/**/!(_)*.svelte')).filter(page => {
        const filters = [
          'slug]',
          '_',
          'private',
          '/src/routes/index.svelte',
        ]
    
        return !filters.find(filter => page.includes(filter))      
    }).map(page => page.replace('/src/routes', '').replace('/index.svelte', '').replace('+page.svelte', ''))

    let contents = ""
    for (const url of urls) {
        console.log(`adding ${url} to the sitemap`);
        contents = contents + generateUrl(url)
    }
    return `<?xml version="1.0" encoding="UTF-8"?>\n` +
        `<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">\n` +
        `${contents}` +
        `</urlset>\n`
} 

export const prerender = true;

export const GET = async () => {
    return new Response(getSiteMap(), {
        headers: {
            "content-type": "text/xml"
        }
    });
}